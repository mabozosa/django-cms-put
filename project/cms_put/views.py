from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Contenido


# Create your views here.

formulario = """
<h3>No existe el valor en la base de datos para esta clave.</h3>
<p>Introdúcelo a continuación:
<p>
 <form action ="" method ="POST">
    Valor: <input type="text" name="valor">
    <input type="submit" value="Enviar">
 </form>
"""

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":  # metodo PUT
        # Guarda el cuerpo de la peticion HTTP (PUT)
        valor = request.body.decode('utf-8')
        try:  # Actualiza el contenido para una clave ya existente
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        # Se crea nuevo contenido si no existe la clave (valor del recurso)
        except Contenido.DoesNotExist:
            # Nuevo objeto contenido con su clave y valor
            c = Contenido(clave=llave, valor=valor)
        c.save()  # Guarda el contenido en la base de datos

    if request.method == "POST":  # Metodo POST
        valor = request.POST['valor']  # Guarda el campo valor del formulario
        try:  # Actualiza el contenido para una clave ya existente
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        # Se crea nuevo contenido si no existe la clave
        except Contenido.DoesNotExist:
            # Nuevo objeto contenido con su clave y valor
            c = Contenido(clave=llave, valor=valor)
        c.save()  # Guarda el contenido en la base de datos

    # metodo GET
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "<h3>" + contenido.valor + "</h3>"
    except Contenido.DoesNotExist:
        respuesta = formulario
    return HttpResponse(respuesta)
